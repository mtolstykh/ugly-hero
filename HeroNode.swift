//
//  HeroNode.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/6/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class HeroNode: SKSpriteNode {
    
    let heroName: String
    let heroAttributes: HeroAttributes
    var textures = [SKTexture]()
    var texturesCapsula = [SKTexture]()
    var walkAction: SKAction
    var walkWithCapsula: SKAction
    var capsula: Bool = false
    
    init(attributes: HeroAttributes, atlas: String, texture: String, position: CGPoint) {
        let atl = SKTextureAtlas(named: atlas)
        let atlCapsula = SKTextureAtlas(named: atlas + "-capsula")
        textures = [SKTexture]()
        texturesCapsula = [SKTexture]()
        
        for i in 1...atl.textureNames.count {
            textures.append(SKTexture(imageNamed: "\(atlas)-\(i).png"))
        }
        
        for i in 1...atlCapsula.textureNames.count {
            texturesCapsula.append(SKTexture(imageNamed: "\(atlas)-capsula-\(i).png"))
        }
        
        self.heroName = texture
        self.heroAttributes = attributes
        self.walkAction = SKAction.animate(with: textures, timePerFrame: heroAttributes.animationDuration)
        self.walkWithCapsula = SKAction.animate(with: texturesCapsula, timePerFrame: heroAttributes.animationDuration)
        
        let t = SKTexture(imageNamed: texture)
        
        super.init(texture: t, color: UIColor.black, size: t.size())
        
        name = heroName
        self.position = position
        physicsBody = SKPhysicsBody(
            rectangleOf: CGSize (
                width: size.width * attributes.physicsBodyDimensionRatio,
                height: size.height * attributes.physicsBodyDimensionRatio
            )
        )
        physicsBody?.mass = attributes.mass
        physicsBody?.affectedByGravity = true
        physicsBody?.isDynamic = true
        physicsBody?.categoryBitMask = PhysicsCategory.Hero
        physicsBody?.collisionBitMask = PhysicsCategory.Floor | PhysicsCategory.Monster
        physicsBody?.contactTestBitMask = PhysicsCategory.Floor | PhysicsCategory.Monster
        physicsBody?.allowsRotation = false
        zPosition = attributes.zPosition
    }
    
    func walk() {
        self.removeAllActions()
        self.run(SKAction.repeatForever(walkAction), withKey: "walk")
    }
    
    func walkCapsula()
    {
        self.removeAllActions()
        self.run(SKAction.repeat(walkWithCapsula, count: 25)) {
            self.capsula = false
            self.walk()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func jump() {
        physicsBody?.applyImpulse(CGVector(dx: 0, dy: heroAttributes.jumpForse))
    }
    
    func shoot() {
        
    }
    
    func stop() {
        self.removeAction(forKey: "walk")
    }
}
