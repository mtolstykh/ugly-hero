//
//  HeroAttributes.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/6/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class HeroAttributes {
    let physicsBodyDimensionRatio: CGFloat
    let zPosition: CGFloat
    let animationDuration: Double
    let jumpForse: CGFloat
    let mass: CGFloat
    
    init(
        physicsBodyDimensionRatio: CGFloat,
        zPosition: CGFloat,
        animationDuration: Double,
        jumpForse: CGFloat,
        mass: CGFloat
    ) {
        self.physicsBodyDimensionRatio = physicsBodyDimensionRatio
        self.zPosition = zPosition
        self.animationDuration = animationDuration
        self.jumpForse = jumpForse
        self.mass = mass
    }
}
