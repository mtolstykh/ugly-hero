//
//  Bullet.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/6/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class Bullet: SKSpriteNode {
    
    private static let Texture = "bullet"
    
    init (position: CGPoint) {
        let texture = SKTexture(imageNamed: Bullet.Texture)
        
        super.init(
            texture: texture,
            color: UIColor.black,
            size: texture.size()
        )
        setScale(0.3)
        name = Bullet.Texture
        self.position = position
        physicsBody = SKPhysicsBody(rectangleOf: self.size)
        physicsBody?.isDynamic = true
        physicsBody?.affectedByGravity = false
        physicsBody?.categoryBitMask = PhysicsCategory.Bullet
        physicsBody?.collisionBitMask = PhysicsCategory.Monster
        physicsBody?.contactTestBitMask = PhysicsCategory.Monster
        zPosition = 0.2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fly() {
        physicsBody?.applyImpulse(CGVector(dx: SceneConstants.bulletSpeed, dy: 0))
    }
}
