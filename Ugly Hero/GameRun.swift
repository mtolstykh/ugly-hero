//
//  GameRun.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 17/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import Foundation

class GameRun: NSObject, NSCoding
{
    var Monsters: Int
    var Meters: Float
    var Coins: Int
    
    // not savable data
    var MonstersJump = 0
    var MonstersFly = 0
    var MonstersGrawer = 0
    var MonstersFrequen = 0
    var MonstersMazzgerHappy = 0
    var MonstersMazzgerAngry = 0
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.Monsters = aDecoder.decodeInteger(forKey: "Monsters")
        self.Meters = aDecoder.decodeFloat(forKey: "Meters")
        self.Coins = aDecoder.decodeInteger(forKey: "Coins")
    }
    
    override init()
    {
        self.Coins = 0
        self.Meters = 0.0
        self.Monsters = 0
    }

    init(monsters: Int, meters: Float, coins: Int)
    {
        self.Coins = coins
        self.Meters = meters
        self.Monsters = monsters
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.Monsters, forKey: "Monsters")
        aCoder.encode(self.Meters, forKey: "Meters")
        aCoder.encode(self.Coins, forKey: "Coins")
    }
}
