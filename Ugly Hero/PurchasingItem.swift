//
//  PurchasingItem.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 27/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit
import StoreKit

class PurchasingItem: UIButton
{
    let purchasingImageView: UIImageView
    let coinsLabel: UILabel
    let costLabel: UILabel
    let product: SKProduct
    
    init(coins: String, imageName: String, cost: String, gs: GameScene, product: SKProduct)
    {
        let width = (gs.view?.frame.width)! / 5
        let height = (gs.view?.frame.height)! / 3
        
        self.purchasingImageView = UIImageView(image: UIImage(named: imageName))
        self.purchasingImageView.frame.size = CGSize(width: width - 10, height: height - 10)
        
        self.coinsLabel = UILabel(
            frame: CGRect(
                x: 5,
                y: self.purchasingImageView.frame.size.height,
                width: width - 10,
                height: 30
            )
        )
        self.coinsLabel.font = UIFont(name: "ugly-hero-font", size: 20.0)
        self.coinsLabel.text = String(coins)
        self.coinsLabel.textColor = UIColor.red
        self.coinsLabel.textAlignment = .center
        
        self.costLabel = UILabel(frame: CGRect(
            x: 5,
            y: self.coinsLabel.frame.origin.y +
                self.coinsLabel.frame.size.height/2,
            width: width - 10,
            height: 30
        ))
        self.costLabel.font = UIFont(name: "ugly-hero-font", size: 20.0)
        self.costLabel.text = cost
        self.costLabel.textColor = UIColor.white
        self.costLabel.textAlignment = .center
        
        self.product = product
        
        super.init(frame: CGRect(
            x: 5,
            y: 5,
            width: width-10,
            height: height-10
        ))
        
//        self.setImage(UIImage(named: imageName), for: .normal)
        
        self.addSubview(purchasingImageView)
        self.addSubview(coinsLabel)
        self.addSubview(costLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
