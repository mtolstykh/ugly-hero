//
//  PurchasingView.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 26/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit
import StoreKit

class PurchasingView: UIView
{
    var viewConstraints = [NSLayoutConstraint]()
    
    init(frame: CGRect, gs: GameScene) {
        super.init(frame: frame)
        
        self.frame.size = CGSize(width: frame.width, height: frame.height / 3)
        var buttons = [PurchasingItem]()
        
        
        for i in 0..<products.count {
            let product = products[i]
            let numberFormatter = NumberFormatter()
            let price = product.price
            let locale = product.priceLocale
            numberFormatter.numberStyle = .currencyISOCode
            numberFormatter.locale = locale
            
            let purchItem = PurchasingItem(
                coins: product.localizedTitle,
                imageName: "purchasing-\(i + 1).png",
                cost: numberFormatter.string(from: price)!,
                gs: gs,
                product: product
            )
            if i > 0 {
                let prev = buttons[i-1].frame
                purchItem.frame.origin = CGPoint(x: prev.origin.x + prev.size.width + 10, y: 5)
            }
            purchItem.accessibilityIdentifier = product.productIdentifier
            if let gvc = gs.view?.window?.rootViewController as? GameViewController {
                purchItem.addTarget(gvc.purchasingManager, action: #selector(PurchasingManager.tapButton), for: .touchUpInside)
            }
            buttons.append(purchItem)
            
            self.addSubview(purchItem)
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
