//
//  Frequen.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/5/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class Frequen: MonsterNode {
    private static let AtlasName = "frequen-iddle"
    private static let TextureName = "frequen"
    
    init(position: CGPoint) {
        let frequenSize = CGSize(width: 60, height: 55)
        super.init(
            atlas: Frequen.AtlasName,
            texture: Frequen.TextureName,
            size: frequenSize,
            position: position
        )
        
        setScale(0.1)
        
        physicsBody = SKPhysicsBody(circleOfRadius: size.width * 0.25)
        physicsBody?.mass = 0.1
        physicsBody?.affectedByGravity = false
        physicsBody?.isDynamic = false
        physicsBody?.allowsRotation = false
        physicsBody?.categoryBitMask = PhysicsCategory.Monster
        physicsBody?.collisionBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero | PhysicsCategory.Bullet | PhysicsCategory.Monster
        physicsBody?.contactTestBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero | PhysicsCategory.Bullet | PhysicsCategory.Monster
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
