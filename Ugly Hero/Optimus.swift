//
//  Optimus.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/6/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class Optimus: HeroNode {
    private static let AtlasName = "optimus-walk"
    private static let TextureName = "optimus"
    
    init(position: CGPoint) {
       
        let attributes = HeroAttributes(
            physicsBodyDimensionRatio: 1.0,
            zPosition: 0.2,
            animationDuration: 0.2,
            jumpForse: SceneConstants.jumpImpulse,
            mass: 0.2)
        
        super.init(
            attributes: attributes,
            atlas: Optimus.AtlasName,
            texture: Optimus.TextureName,
            position: position
        )
        
        setScale(0.05)
        self.position = CGPoint(x: position.x + size.width, y: position.y)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
