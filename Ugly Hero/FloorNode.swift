//
//  FloorNode.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 05/03/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class FloorNode: SKSpriteNode {
    
    private static let NodeName = "Floor"
    
    init(frameSize: CGSize) {
        super.init(
            texture: SKTexture(),
            color: UIColor.brown,
            size: CGSize(
                width: frameSize.width,
                height: frameSize.height / 4
            )
        )
        
        self.position = CGPoint (
            x: 0,
            y: -frameSize.height / 2
        )
        name = FloorNode.NodeName
        physicsBody = SKPhysicsBody(
            rectangleOf: CGSize(
                width: size.width,
                height: size.height / 2
            )
        )
        
        physicsBody?.affectedByGravity = false
        physicsBody?.isDynamic = false
        physicsBody?.categoryBitMask = PhysicsCategory.Floor
        physicsBody?.collisionBitMask = PhysicsCategory.Hero | PhysicsCategory.Monster
        physicsBody?.contactTestBitMask = PhysicsCategory.Hero | PhysicsCategory.Monster
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
