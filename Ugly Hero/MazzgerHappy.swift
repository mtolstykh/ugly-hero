//
//  MazzgerHappy.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 05/03/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class MazzgerHappy: MonsterNode {
    private static let AtlasName = "mazzger-happy-iddle"
    private static let TextureName = "mazzger-happy"
    
    init(position: CGPoint) {
        let mazzgerSize = CGSize(width: 45, height: 50)
        super.init(
            atlas: MazzgerHappy.AtlasName,
            texture: MazzgerHappy.TextureName,
            size: mazzgerSize,
            position: position
        )
        
        physicsBody?.mass = 0.04
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
