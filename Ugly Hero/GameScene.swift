//
//  GameScene.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 19/02/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation
import GameKit
import GoogleMobileAds

struct GameState {
    static let GameOver = 0
    static let Paused = 1
    static let Running = 2
    static let Launched = 3
}

class GameScene: SKScene
{
    static let fontName = "ugly-hero-font"
    
    static var heroCanJump = true
    static var gameOnPause = true
    static var sceneLaunched = 0
    
    let collisionDetector = CollisionDetector()
    let gameCenterManager = GameCenterManager()
    
    var interstitial: GADInterstitial!
    
    var state = GameState.Launched
    var cameraNode = SKCameraNode()
    var hero: HeroNode?
    var frameWidth: CGFloat = 0
    var cameraPositionOffset: CGFloat = 0
    var heroPosition: CGPoint = CGPoint(x: 0,y: 0)
    
    var kills: Int = 0
    var meters: Float = 0.0
    var coins: Int = 0
    
    var killsLabel: KillsLabel = KillsLabel()
    var metersLabel: MetersLabel = MetersLabel()
    var coinsLabel: CoinsLabel = CoinsLabel()
    var tapAnywhereLabel: TapAnyLabel = TapAnyLabel()
    
    var lvlBgNodes = [Level1Landscape]()
    var bullets = [Bullet]()
    
    var xStart: CGFloat = 0
    var backgroundMusic: SKAudioNode = SKAudioNode()
    var startMusic: SKAudioNode = SKAudioNode()
    
    let formatter = NumberFormatter()
    
    override func didMove(to view: SKView) {
        NotificationCenter.default.addObserver(self, selector: #selector(pause), name: NSNotification.Name("PauseGameScene"), object: nil)
        
        formatter.minimumIntegerDigits = 4
        formatter.minimumFractionDigits = 1
        
        collisionDetector.gameScene = self
        self.physicsWorld.contactDelegate = collisionDetector
        
        frameWidth = view.frame.size.width
        xStart = frameWidth * 2
        
        scene?.size = CGSize(
            width: view.frame.size.width * 6,
            height: view.frame.size.height
        )
        
        let frameSize = CGSize(width: frameWidth, height: size.height)
        
        lvlBgNodes = Level1Landscape.getInstance(frameSize: frameSize)
        for lvl in lvlBgNodes {
            self.addChild(lvl)
        }
        
        self.addChild(cameraNode)
        camera = cameraNode
        cameraNode.position = CGPoint(
            x: xStart + frameWidth / 2 ,
            y: size.height / 2
        )
        
        hero = Optimus(position: CGPoint(x: xStart, y: lvlBgNodes[0].floorHeight))
        heroPosition = (hero?.position)!
        self.addChild(hero!)
        
        cameraPositionOffset = cameraNode.position.x - (hero?.position.x)!
        
        if state == GameState.Launched {
            showTapLabel()
        }
        if state == GameState.Running {
            addGameLabel()
            addPauseButton()
        }
        
        if let musicURL = Bundle.main.url(forResource: "start-music", withExtension: "mp3") {
            startMusic = SKAudioNode(url: musicURL)
            startMusic.autoplayLooped = true
            addChild(startMusic)
        }
        
//      ca-app-pub-3940256099942544/4411468910
        //ca-app-pub-7235301939697157/9547142222 ad unit id
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-7235301939697157/9547142222")
        let request = GADRequest()
//        request.testDevices = ["4b43e7117d486ebe6a36a3d085c34daa", kGADSimulatorID]
        interstitial.load(request)

    }
    
    func addGameLabel()
    {
        if let kln = self.childNode(withName: KillsLabel.killsLabelNodeName) {
            kln.removeFromParent()
        }
        
        if let mln = self.childNode(withName: MetersLabel.metersLabelNodeName) {
            mln.removeFromParent()
        }
        
        if let cln = self.childNode(withName: CoinsLabel.coinsLabelNodeName) {
            cln.removeFromParent()
        }
        
        killsLabel = KillsLabel(
            position: CGPoint(
                x: xStart + 10,
                y: size.height - 20
            ),
            kills: self.kills
        )
        addChild(killsLabel)
        
        metersLabel = MetersLabel(
            position: CGPoint(
                x: xStart + 10,
                y: size.height - 35
            ),
            meters: self.meters
        )
        addChild(metersLabel)
        
        coinsLabel = CoinsLabel(
            position: CGPoint(
                x: xStart + 10,
                y: size.height - 50
            ),
            coins: self.coins
        )
        addChild(coinsLabel)
        
        
    }
    
    func showTapLabel() {
        tapAnywhereLabel = TapAnyLabel(
            position: CGPoint(
                x: xStart + frameWidth / 2,
                y: (scene?.size.height)! / 2
            )
        )
        addChild(tapAnywhereLabel)
        
        let flashLabelAction = SKAction.sequence(
            [
                SKAction.fadeIn(withDuration: 0.3),
                SKAction.wait(forDuration: 0.3),
                SKAction.fadeOut(withDuration: 0.3)
            ]
        )
        
        tapAnywhereLabel.run(SKAction.repeatForever(flashLabelAction))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if state == GameState.Running {
            if let touchPosition = touches.first?.location(in: self) {
                let touchOffsetNormal = (frameWidth * 3) - frameWidth/2
                
                if (touchPosition.x - touchOffsetNormal) > 0.0 {
                    jump()
                }
                
                if (touchPosition.x - touchOffsetNormal) < 0.0 {
                    shoot()
                }
            }
        }
        
        if state == GameState.Launched {
            tapAnywhereLabel.removeFromParent()
            runGame()
        }
    }
    
    func runGame() {
        addPauseButton()
        let profile = getPlayerProfile()
        if profile.helpers.contains(Helpers.Bomb){
            let bombButton = getBombButton()
            scene?.view?.addSubview(bombButton)
        }
        if profile.helpers.contains(Helpers.Capsula) {
            let capsulaButton = getCapsulaButton()
            scene?.view?.addSubview(capsulaButton)
        }
        state = GameState.Running
        GameScene.gameOnPause = false
        self.view?.isPaused = false
        self.isPaused = false
        
        startMusic.removeFromParent()
        
        if let musicURL = Bundle.main.url(forResource: "uh-bg-music", withExtension: "m4a") {
            backgroundMusic = SKAudioNode(url: musicURL)
            backgroundMusic.autoplayLooped = true
            addChild(backgroundMusic)
        }
        
        for levelNode in lvlBgNodes {
            levelNode.runMonsters()
        }
        
        addGameLabel()
        
        hero?.walk()
    }
    
    var levelMoveCount = 0
    
    override func update(_ currentTime: TimeInterval)
    {
        
        switch state {
        case GameState.GameOver:
            //            backgroundMusic.run(SKAction.stop())
            //show result menu
            break;
        case GameState.Launched:
            //show paused scene
            //show tap label
            //            showTapLabel()
            break;
        case GameState.Paused:
            //show pause menu
            //stop all monsters
            break;
        case GameState.Running:
            //hide pause menu
            //hide tap label
            //start monsters moving
            
            meters += 0.1;
            metersLabel.text = "Distance: " + formatter.string(for: meters)! + "m"
            
            if let gsView = view {
                if let gameController = gsView.window?.rootViewController as? GameViewController {
                    let gameRun = GameRun (monsters: kills, meters: meters, coins: coins)
                    
                    if (gameController.achievementManager.process(gameRun: gameRun))
                    {
                        var achs = [GKAchievement]()
                        
                        for achName in gameController.achievementManager.completed {
                            let split = achName.components(separatedBy: ".")
                            let indexValue = split.last
                            var achParts = split.dropLast()
                            var achieveId = split.first
                            achParts = achParts.dropFirst()
                            
                            achParts.forEach { part in
                                achieveId?.append("." + part)
                            }
                            
                            let achieveVal = AchievementsManager.achievements[achieveId!]?[Int(indexValue!)!]
                            var achievementID = ""
                            if let intyVal = achieveVal as? Int{
                                achievementID = "\(achieveId!).\(intyVal)"
                            } else if let floatyVal = achieveVal as? Float {
                                achievementID = "\(achieveId!).\(Int(floatyVal))"
                            }
                            
                            print("Complete achievement \(achievementID)")
                            let a = GKAchievement(identifier: achievementID)
                            a.showsCompletionBanner = true
                            a.percentComplete = 100.0
                            achs.append(a)
                            
                            GKAchievement.report(achs) { error in
                                if (error != nil)
                                {
                                    NSLog("Error in reporting achievements: \(error!)");
                                } else {
                                    gameController.playerProfile.totalCoins += Int(arc4random_uniform(UInt32(50))) + 50
                                }
                            }
                        }
                    }
                    
                }
            }
            
            for b in bullets {
                if b.position.x > cameraNode.position.x + frameWidth/2 {
                    b.removeFromParent()
                    if let indexToRemove = bullets.index(of: b) {
                        bullets.remove(at: indexToRemove)
                    }
                }
            }
            
            for lvl in lvlBgNodes {
                if (lvl.position.x - 5.0) <= (lvl.size.width + lvl.size.width / 4) {
                    lvl.updateConfig(count: levelMoveCount)
                    lvl.position.x += lvl.size.width * 4
                    levelMoveCount += 1
                    if levelMoveCount == 6 {
                        levelMoveCount = 0
                    }
                }
                
                lvl.position.x -= SceneConstants.runningSpeed
            }
            
            if hero?.position.x != heroPosition.x {
                hero?.run(SKAction.moveTo(x: heroPosition.x, duration: 3))
            }
            
            break;
        default:
            
            break;
        }
    }
    
    func replay() {
        let reveal : SKTransition = SKTransition.fade(withDuration: 0.5)
        let scene = GameScene(size: self.view!.bounds.size)
        scene.state = GameState.Running
        scene.scaleMode = .aspectFill
        self.view?.presentScene(scene, transition: reveal)
        if let subviews = view?.subviews {
            for sv in subviews {
                sv.removeFromSuperview()
            }
        }
        scene.runGame()
    }
    
    func jump()
    {
        if (hero?.position.y)! < lvlBgNodes[0].floorHeight + 1.0 {
            hero?.jump()
            hero?.run(SKAction.playSoundFileNamed("jump-2.mp3", waitForCompletion: true))
        }
    }
    
    func shoot()
    {
        let b = Bullet(position: (hero?.position)!)
        addChild(b)
        bullets.append(b)
        b.fly()
        b.run(SKAction.playSoundFileNamed("uh-shoot-1.mp3", waitForCompletion: true))
    }
    
    func addPauseButton()
    {
        let pauseButton = UIButton()
        let bgImage = UIImage(named: "pause.png")
        let bgImagePressed = UIImage(named: "pause-pressed.png")
        pauseButton.frame.size = CGSize(width: 30, height: 30)
        pauseButton.frame.origin = CGPoint(x: (view?.frame.size.width)! - pauseButton.frame.size.width - 10, y: 15)
        pauseButton.setImage(bgImage, for: UIControlState.normal)
        pauseButton.setImage(bgImagePressed, for: UIControlState.highlighted)
        pauseButton.imageView?.contentMode = .scaleAspectFit
        
        pauseButton.addTarget(self, action: #selector(GameScene.pause), for: .touchUpInside)
        
        self.view?.addSubview(pauseButton)
        
    }
    
    var pauseView: GamePauseView?
    
    func quitShop() {
        if pauseView != nil {
            pauseView?.removeFromSuperview()
        }
    }
    
    func shop()
    {
        if pauseView == nil {
            pauseView = GamePauseView(frame: (self.view?.frame)!, gs: self)
        }
        
        pauseView?.quitShopButton.isEnabled = true
        pauseView?.quitShopButton.isHidden = false
        
        pauseView?.quitButton.isEnabled = false
        pauseView?.quitButton.isHidden = true
        
        pauseView?.restartButton.isEnabled = false
        pauseView?.restartButton.isHidden = true
        
        pauseView?.resumeButton.isEnabled = false
        pauseView?.resumeButton.isHidden = true

        
        self.view?.addSubview(pauseView!)
    }
    
    func pause()
    {
        if state == GameState.Running {
            state = GameState.Paused
            self.isPaused = true
            self.view?.isPaused = true
            
            //show menu
            pauseView = GamePauseView(frame: (self.view?.frame)!, gs: self)
            
            self.view?.addSubview(pauseView!)
        }
    }
    
    func resume() {
        if state == GameState.Paused {
            if let pv = pauseView
            {
                if pv.superview != nil
                {
                    pv.removeFromSuperview()
                }
            }
            
            state = GameState.Running
            self.isPaused = false
            self.view?.isPaused = false
        }
    }
    
    func quit() {
        GameScene.gameOnPause = false
        self.view?.isPaused = false
        self.isPaused = false
        
        hero?.stop()
        let reveal : SKTransition = SKTransition.fade(withDuration: 0.5)
        let scene = GameScene(size: self.view!.bounds.size)
        scene.state = GameState.Launched
        scene.scaleMode = .aspectFill
        self.view?.presentScene(scene, transition: reveal)
        if let subviews = view?.subviews {
            for sv in subviews {
                sv.removeFromSuperview()
            }
        }
    }
    
    func monsterRoom()
    {
        let monsterRoomView = SKView(frame: (self.view?.frame)!)
        monsterRoomView.frame.origin = CGPoint(x: 0, y: 0)
        let scene = MonsterRoomScene(size: (self.view?.frame.size)!)
        self.view?.addSubview(monsterRoomView)
        monsterRoomView.presentScene(scene)
    }
    
    func getPlayerProfile() -> PlayerProfile
    {
        var profile = PlayerProfile()
        
        if let gsView = self.view {
            if let gameController = gsView.window?.rootViewController as? GameViewController {
                profile = gameController.playerProfile
            }
        }
        return profile
    }
    
    func levelsRoom()
    {
        
    }
    
    func heroesRoom()
    {
        
    }
    
    func achievementsRoom()
    {
        let achievementsRoomView = SKView(frame: (self.view?.frame)!)
        achievementsRoomView.backgroundColor = UIColor.white
        achievementsRoomView.frame.origin = CGPoint(x: 0, y: 0)
        let scene = AchievementsRoomScene(size: (self.view?.frame.size)!)
        scene.backgroundColor = UIColor.white
        self.view?.addSubview(achievementsRoomView)
        achievementsRoomView.presentScene(scene)
    }
    
    static func getCoinImageView() -> UIImageView
    {
        let coinImage = UIImage(named: "coin.png")
        let coinImageView = UIImageView(image: coinImage)
        coinImageView.contentMode = .scaleAspectFit
        coinImageView.frame.size = CGSize(
            width: (coinImage?.size.width)! / 25,
            height: (coinImage?.size.height)! / 25
        )
        
        return coinImageView
    }
    
    func useBomb()
    {
        scene?.isPaused = true
        let profile = getPlayerProfile()
        let bombIndex = profile.helpers.index(of: Helpers.Bomb)
        profile.helpers.remove(at: bombIndex!)
        let bomb = Bomb()
        for sv in (scene?.view?.subviews)! {
            if sv.accessibilityIdentifier == "bomb-button" {
                bomb.position = cameraNode.position
                sv.removeFromSuperview()
            }
        }
        scene?.addChild(bomb)
        scene?.isPaused = false
        bomb.run(SKAction.rotate(byAngle: 180, duration: 100.0))
    }
    
    func buyBomb(sender: UIButton)
    {
        let profile = getPlayerProfile()
        if profile.totalCoins > 100 &&
            !profile.helpers.contains(Helpers.Bomb)
        {
            profile.totalCoins -= 100
            //play sound for withdraw
            sender.isEnabled = false
            profile.helpers.append(Helpers.Bomb)
            let bombButton = getBombButton()
            scene?.view?.addSubview(bombButton)
            scene?.view?.sendSubview(toBack: bombButton)
        } else {
            // suggest to get coins
        }
    }
    
    func useCapsula(sender: UIButton) {
        hero?.capsula = true
        let profile = getPlayerProfile()
        let capsulaIndex = profile.helpers.index(of: Helpers.Capsula)
        profile.helpers.remove(at: capsulaIndex!)
        sender.removeFromSuperview()
        hero?.walkCapsula()
    }
    
    func buyCapsula(sender: UIButton)
    {
        let profile = getPlayerProfile()
        if profile.totalCoins > 300 &&
            !profile.helpers.contains(Helpers.Capsula)
        {
            profile.totalCoins -= 300
            //play sound for withdraw
            sender.isEnabled = false
            profile.helpers.append(Helpers.Capsula)
            let capsulaButton = getCapsulaButton()
            scene?.view?.addSubview(capsulaButton)
            scene?.view?.sendSubview(toBack: capsulaButton)
        } else {
            
        }
    }
    
    func useLife() {
        for sv in (view?.subviews)! {
            if let gov = sv as? GameOverView {
                gov.removeFromSuperview()
            }
        }
        addGameLabel()
        self.state = GameState.Running
        GameScene.gameOnPause = false
        view?.isPaused = false
        isPaused = false
        let profile = getPlayerProfile()
        let lifeIndex = profile.helpers.index(of: Helpers.Life)
        profile.helpers.remove(at: lifeIndex!)
    }
    
    func buyLife(sender: UIButton)
    {
        let profile = getPlayerProfile()
        if profile.totalCoins > 200 &&
            !profile.helpers.contains(Helpers.Life)
        {
            profile.totalCoins -= 200
            //play sound for withdraw
            sender.isEnabled = false
            profile.helpers.append(Helpers.Life)
        } else {
            
        }
    }
    
    func getBombButton() -> UIButton
    {
        let bombImage = UIImage(named: "bomb.png")
        let bombButton = UIButton()
        bombButton.accessibilityIdentifier = "bomb-button"
        bombButton.frame.size = CGSize(width: (bombImage?.size.width)! / 2, height: (bombImage?.size.height)! / 2)
        bombButton.setImage(bombImage, for: UIControlState.normal)
        bombButton.frame.origin = CGPoint(
            x: frameWidth / 2 + bombButton.frame.size.width + 2,
            y: 10
        )
        bombButton.addTarget(self, action: #selector(useBomb), for: .touchUpInside)
        return bombButton
    }
    
    func getCapsulaButton() -> UIButton
    {
        let capsulaImage = UIImage(named: "capsula.png")
        let capsulaButton = UIButton()
        capsulaButton.frame.size = CGSize(
            width: (capsulaImage?.size.width)! / 2,
            height: (capsulaImage?.size.height)! / 2
        )
        capsulaButton.setImage(capsulaImage, for: UIControlState.normal)
        capsulaButton.frame.origin = CGPoint(
            x: frameWidth / 2 - capsulaButton.frame.size.width - 2,
            y: 10
        )
        capsulaButton.addTarget(self, action: #selector(useCapsula), for: .touchUpInside)
        return capsulaButton
    }
}
