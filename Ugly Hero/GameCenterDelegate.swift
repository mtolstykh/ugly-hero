//
//  GameCenterDelegate.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 20/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit

class GameCenterDelegate: NSObject, GKGameCenterControllerDelegate
{
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController)
    {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
}
