//
//  MetersLabel.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 17/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class MetersLabel: SKLabelNode
{
    static let metersLabelNodeName = "MetersLabel"
    
    override init()
    {
        super.init()
    }

    init(position: CGPoint, meters: Float)
    {
        super.init(fontNamed: GameScene.fontName)
        self.text = "Distance: " + String(format: "%03f", meters) + "m"
        self.name = MetersLabel.metersLabelNodeName
        self.horizontalAlignmentMode = .left
        self.fontSize = 15
        self.zPosition = 0.2
        self.color = UIColor.white
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
