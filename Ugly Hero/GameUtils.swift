//
//  GameUtils.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/6/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

struct SceneConstants {
    static let framesPerScene = 4
    static var runningSpeed: CGFloat = 2.0
    static let bulletSpeed: CGFloat = 40.0
    static let jumpImpulse: CGFloat = 150
}

struct PhysicsCategory {
    static let Hero: UInt32 = 0x1 << 1
    static let Floor: UInt32 = 0x1 << 2
    static let Bullet: UInt32 = 0x1 << 3
    static let Monster: UInt32 = 0x1 << 4
    static let Coin: UInt32 = 0x1 << 5
    static let Bomb: UInt32 = 0x1 << 6
}

struct Helpers {
    static let Bomb = "BOMB"
    static let Capsula = "CAPSULA"
    static let Life = "LIFE"
}
