//
//  PurchasingManager.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 29/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import StoreKit

let sharedSecret = "375ce2302e094d18b6fd8f8755664b74"
let bundleID = "com.braincrisys.UglyHero"
let leaderBoardDistance = "com.braincrisys.UglyHero.distanse"

enum RegisteredPurchase : String {
    case Coins1000 = "1000.coins"
    case Coins3000 = "3000.coins"
    case Coins5000 = "5000.coins"
    case Coins10000 = "10000.coins"
    case Coins100000 = "100000.coins"
}

let coinsBox = [
    "com.braincrisys.UglyHero." + RegisteredPurchase.Coins1000.rawValue: 1000,
    "com.braincrisys.UglyHero." + RegisteredPurchase.Coins3000.rawValue: 3000,
    "com.braincrisys.UglyHero." + RegisteredPurchase.Coins5000.rawValue: 5000,
    "com.braincrisys.UglyHero." + RegisteredPurchase.Coins10000.rawValue: 10000,
    "com.braincrisys.UglyHero." + RegisteredPurchase.Coins100000.rawValue: 100000
]

class NetworkActivityIndicatorManager : NSObject {
    
    private static var loadingCount = 0
    
    class func NetworkOperationStarted() {
        if loadingCount == 0 {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        loadingCount += 1
    }
    class func networkOperationFinished(){
        if loadingCount > 0 {
            loadingCount -= 1
            
        }
        
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
        }
    }
}

let registeredProducts = [
    RegisteredPurchase.Coins1000,
    RegisteredPurchase.Coins3000,
    RegisteredPurchase.Coins5000,
    RegisteredPurchase.Coins10000,
    RegisteredPurchase.Coins100000
]

var products: [SKProduct] = []

class PurchasingManager {
    
    let appleValidator: ReceiptValidator
    let viewController: UIViewController

    init(rootViewController: UIViewController) {
        self.viewController = rootViewController
        self.appleValidator = AppleReceiptValidator(service: .production)
    }

    func getInfo(purchase : RegisteredPurchase) {
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.retrieveProductsInfo([bundleID + "." + purchase.rawValue], completion: {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            print(result.retrievedProducts)
            if !result.retrievedProducts.isEmpty {
                if let product = result.retrievedProducts.first {
                    products.append(product)
                }
            }
//            self.viewController.showAlert(alert: self.viewController.alertForProductRetrievalInfo(result: result))
        })
    }
    
    @objc func tapButton(sender:UIButton)
    {
        if let purchasingItem = sender as? PurchasingItem {
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
            activityIndicator.frame.origin = CGPoint(
                x: sender.frame.width / 2 - activityIndicator.frame.width / 2,
                y: sender.frame.height / 2 - activityIndicator.frame.height / 2)
            sender.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            purchase(purchase: purchasingItem.product, indicator: activityIndicator)
        }
    }
    
    func purchase(purchase : SKProduct, indicator: UIActivityIndicatorView) {
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.purchaseProduct(purchase.productIdentifier, completion: {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let product) = result {
                
                // Proccess purchasings
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
                
                if let gvc = self.viewController.view.window?.rootViewController as? GameViewController {
                    gvc.playerProfile.adBlock = false
                }
                self.viewController.showAlert(alert: self.viewController.alertForPurchaseResult(result: result))
                
                indicator.stopAnimating()
            } else {
                indicator.stopAnimating()
            }
            
        })
        
    }
    func restorePurchases() {
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.restorePurchases(atomically: true, completion: {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            for product in result.restoredProducts {
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
            }
            
//            self.viewController.showAlert(alert: self.viewController.alertForRestorePurchases(result: result))
            
        })
    }
    func verifyReceipt() {
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.verifyReceipt(using: appleValidator, password: sharedSecret, completion: {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            self.viewController.showAlert(alert: self.viewController.alertForVerifyReceipt(result: result))
            
            if case .error(let error) = result {
                if case .noReceiptData = error {
                    
                    self.refreshReceipt()
                    
                }
            }
            
        })
        
    }
    func verifyPurcahse(product : RegisteredPurchase) {
        NetworkActivityIndicatorManager.NetworkOperationStarted()
        SwiftyStoreKit.verifyReceipt(using: appleValidator, password: sharedSecret, completion: {
            result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result{
            case .success(let receipt):                
                let productID = bundleID + "." + product.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productID, inReceipt: receipt)
                self.viewController.showAlert(alert: self.viewController.alertForVerifyPurchase(result: purchaseResult))
                
            case .error(let error):
                self.viewController.showAlert(alert: self.viewController.alertForVerifyReceipt(result: result))
                if case .noReceiptData = error {
                    self.refreshReceipt()
                    
                }
                
            }
            
            
        })
        
    }
    func refreshReceipt() {
        SwiftyStoreKit.refreshReceipt(completion: {
            result in
            
            self.viewController.showAlert(alert: self.viewController.alertForRefreshRecepit(result: result))
            
        })
        
    }
    
}

extension UIViewController {
    
    func alertWithTitle(title : String, message : String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
        
    }
    func showAlert(alert : UIAlertController) {
        guard let _ = self.presentedViewController else {
            self.present(alert, animated: true, completion: nil)
            return
        }
        
    }
    func alertForProductRetrievalInfo(result : RetrieveResults) -> UIAlertController {
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(title: product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
            
        }
        else if let invalidProductID = result.invalidProductIDs.first {
            return alertWithTitle(title: "Could not retreive product info", message: "Invalid product identifier: \(invalidProductID)")
        }
        else {
            let errorString = result.error?.localizedDescription ?? "Unknown Error. Please Contact Support"
            return alertWithTitle(title: "Could not retreive product info" , message: errorString)
            
        }
        
    }
    func alertForPurchaseResult(result : PurchaseResult) -> UIAlertController {
        
        switch result {
        case .success(let product):
            print("Purchase Succesful: \(product.productId)")
            if let gvc = self as? GameViewController {
                gvc.playerProfile.totalCoins += coinsBox[product.productId]!
            }
            return alertWithTitle(title: "Thank You", message: "Purchase completed")
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown:
                return alertWithTitle(title: "Purchase Failed", message: "Unknown Error. Please Contact Support")
            case .clientInvalid:
                    return alertWithTitle(title: "Purchase Failed", message: "Check your internet connection or try again later.")
            case .paymentInvalid:
                return alertWithTitle(title: "Purchase Failed", message: "Purchase identifier was invalid")
            case .storeProductNotAvailable:
                return alertWithTitle(title: "Purchase Failed", message: "Product not found")
            case .paymentNotAllowed:
                return alertWithTitle(title: "Purchase Failed", message: "You are not allowed to make payments")
            default: return alertWithTitle(title: "Purchase Failed", message: "Unknown Error. Please Contact Support")
            }
        }
    }
    func alertForRestorePurchases(result : RestoreResults) -> UIAlertController {
        if result.restoreFailedProducts.count > 0 {
            print("Restore Failed: \(result.restoreFailedProducts)")
            return alertWithTitle(title: "Restore Failed", message: "Unknown Error. Please Contact Support")
        }
        else if result.restoredProducts.count > 0 {
            return alertWithTitle(title: "Purchases Restored", message: "All purchases have been restored.")
            
        }
        else {
            return alertWithTitle(title: "Nothing To Restore", message: "No previous purchases were made.")
        }
        
    }
    func alertForVerifyReceipt(result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case.success( _):
            return alertWithTitle(title: "Receipt Verified", message: "Receipt Verified Remotely")
        case .error(let error):
            switch error {
            case .noReceiptData:
                return alertWithTitle(title: "Receipt Verification", message: "No receipt data found, application will try to get a new one. Try Again.")
            default:
                return alertWithTitle(title: "Receipt verification", message: "Receipt Verification failed")
            }
        }
    }
    func alertForVerifySubscription(result: VerifySubscriptionResult) -> UIAlertController {
        switch result {
        case .purchased(let expiryDate):
            return alertWithTitle(title: "Product is Purchased", message: "Product is valid until \(expiryDate)")
        case .notPurchased:
            return alertWithTitle(title: "Not purchased", message: "This product has never been purchased")
        case .expired(let expiryDate):
            
            return alertWithTitle(title: "Product Expired", message: "Product is expired since \(expiryDate)")
        }
    }
    func alertForVerifyPurchase(result : VerifyPurchaseResult) -> UIAlertController {
        switch result {
        case .purchased:
            return alertWithTitle(title: "Product is Purchased", message: "Product will not expire")
        case .notPurchased:
            
            return alertWithTitle(title: "Product not purchased", message: "Product has never been purchased")
            
            
        }
        
    }
    func alertForRefreshRecepit(result : RefreshReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            return alertWithTitle(title: "Receipt Refreshed", message: "Receipt refreshed successfully")
        case .error( _):
            return alertWithTitle(title: "Receipt refresh failed", message: "Receipt refresh failed")
        }
    }
    
}
