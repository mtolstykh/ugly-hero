//
//  GameCenterManager.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 24/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit
import StoreKit

class GameCenterManager: UIViewController, SKProductsRequestDelegate
{
    var products: [SKProduct]?
    
    @available(iOS 3.0, *)
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        
        if let prds = products
        {
            if prds.count == 0 {
                print("No products found")
            }
        }
        
        for product in response.invalidProductIdentifiers
        {
            print("Product not found: \(product)")
        }
    }
    
    func calculateAchievments(gameRun: GameRun)
    {
        
    }
    
    func purchasing(sender: PurchasingItem)
    {
        print("Lets purchase item \(String(describing: sender.accessibilityIdentifier))")
    }
    
    func productsInfo()
    {
        if SKPaymentQueue.canMakePayments() {
            let prdIds = Set(["com.braincrisys.UglyHero.1000.coins"])
            let request = SKProductsRequest(productIdentifiers: prdIds)
            request.delegate = self
            request.start()
        } else {
            let alert = UIAlertController(title: "In-App Purchases Not Enabled", message: "Please enable In App Purchase in Settings", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
                
                let url = URL(string: UIApplicationOpenSettingsURLString)
                if let u = url
                {
                    UIApplication.shared.open(u, options: [:], completionHandler: nil)
                }
                
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
