//
//  MonsterFactory.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/13/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class MonsterFactory {
    
    static func generate(instanceOf: String, at: CGPoint) -> MonsterNode {
        switch instanceOf {
            case "grawer": return Grawer(position: at)
            case "frequen": return Frequen(position: at)
            case "mazzger-angry": return MazzgerAngry(position: at)
            case "mazzger-happy": return MazzgerHappy(position: at)
            
            default: return Frequen(position: at)
        }
    }
}
