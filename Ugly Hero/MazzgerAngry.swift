//
//  MazzgerAngry.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/5/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class MazzgerAngry: MonsterNode {
    private static let AtlasName = "mazzger-angry-iddle"
    private static let TextureName = "mazzger-angry"
    
    init(position: CGPoint) {
        let mazzgerSize = CGSize(width: 50, height: 40)
        super.init(
            atlas: MazzgerAngry.AtlasName,
            texture: MazzgerAngry.TextureName,
            size: mazzgerSize,
            position: position
        )
        
        physicsBody?.mass = 0.03
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
