//
//  TapAnyLabel.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 17/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class TapAnyLabel: SKLabelNode
{
    
    override init()
    {
        super.init()
    }

    
    init(position: CGPoint)
    {
        super.init(fontNamed: GameScene.fontName)
        self.position = position
        self.zPosition = 0.3
        self.fontColor = UIColor.orange
        self.text = "Tap anywhere for starting game!"
        self.fontSize = 40
        self.name = "TapAnywhereLabelNode"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
