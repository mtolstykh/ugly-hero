//
//  MonsterNode.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 04/03/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class MonsterNode: SKSpriteNode {
    init(atlas: String, texture: String, size: CGSize, position: CGPoint) {
        let atl = SKTextureAtlas(named: atlas)
        var textures = [SKTexture]()
        for i in 1...atl.textureNames.count {
            textures.append(SKTexture(imageNamed: "\(atlas)-\(i).png"))
        }
        
        let t = SKTexture(imageNamed: texture)
        super.init(texture: t, color: UIColor.black, size: t.size())
        
        setScale(0.15)
        
        name = texture
        self.position = position
        self.zPosition = 0.2
        physicsBody = SKPhysicsBody(circleOfRadius: size.width * 0.15)
        physicsBody?.mass = 0.1
        physicsBody?.affectedByGravity = true
        physicsBody?.isDynamic = true
        physicsBody?.allowsRotation = false
        physicsBody?.categoryBitMask = PhysicsCategory.Monster
        physicsBody?.collisionBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero | PhysicsCategory.Bullet | PhysicsCategory.Monster
        physicsBody?.contactTestBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero | PhysicsCategory.Bullet | PhysicsCategory.Monster
        
        run(SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: 0.1)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func jumping(power: CGFloat, duration: TimeInterval) {
        run(SKAction.repeatForever(SKAction.applyImpulse(CGVector(dx: -10, dy: power), duration: duration)))
    }
}
