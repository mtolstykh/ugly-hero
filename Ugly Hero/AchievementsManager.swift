//
//  AchievementsManager.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 04/05/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

class AchievementsManager
{
    static let monsters = bundleID + ".monsters"
    static let meters = bundleID + ".meters"
    static let monsters_mazzger_happy = bundleID + ".monsters.mazzger.happy"
    static let monsters_mazzger_angry = bundleID + ".monsters.mazzger.angry"
    static let monsters_jump = bundleID + ".monsters.jump"
    static let monsters_fly = bundleID + ".monsters.fly"
    static let monsters_grawer = bundleID + ".monsters.grawer"
    static let monsters_frequen = bundleID + ".frequen"
    
    static let achievements = [
        meters:                     [Float(100.0), Float(200.0), Float(300.0), Float(500.0), Float(1000.0), Float(2000.0)],
        
        monsters:                   [Int(30), Int(50), Int(100)],
        monsters_jump :             [Int(20), Int(40), Int(60)],
        monsters_fly :              [Int(20), Int(40), Int(60)],
        
        monsters_grawer:            [Int(10), Int(20), Int(40)],
        monsters_mazzger_happy:     [Int(10), Int(20), Int(40)],
        monsters_mazzger_angry:     [Int(10), Int(20), Int(40)],
        monsters_frequen:           [Int(10), Int(20), Int(40)]
    ]
    
    var completed = [String]()
    var reported = [String]()
    
    func process(gameRun: GameRun) -> Bool {
        reported.append(contentsOf: completed)
        completed.removeAll()
        
        calc(value: gameRun.Meters,                 key: AchievementsManager.meters)
        calc(value: gameRun.MonstersJump,           key: AchievementsManager.monsters_jump)
        calc(value: gameRun.MonstersFly,            key: AchievementsManager.monsters_fly)
        calc(value: gameRun.MonstersGrawer,         key: AchievementsManager.monsters_grawer)
        calc(value: gameRun.MonstersMazzgerAngry,   key: AchievementsManager.monsters_mazzger_angry)
        calc(value: gameRun.MonstersMazzgerHappy,   key: AchievementsManager.monsters_mazzger_happy)
        calc(value: gameRun.MonstersFrequen,        key: AchievementsManager.monsters_frequen)
        calc(value: gameRun.Monsters,               key: AchievementsManager.monsters)
        
        return !completed.isEmpty
    }
    
    func calc(value: Any, key: String) {
        if let values = AchievementsManager.achievements[key] {
            for i in 0..<values.count {
                let completedName = key + ".\(i)"
                if let v = value as? Float {
                    if v >= values[i] as! Float {
                        //report about key
                        if !reported.contains(completedName) {
                            completed.append(completedName)
                        }
                    }
                } else if let v = value as? Int {
                    if v >= values[i] as! Int {
                        //report about key
                        if !reported.contains(completedName) {
                            completed.append(completedName)
                        }
                    }
                }
            }
        }
    }
}
