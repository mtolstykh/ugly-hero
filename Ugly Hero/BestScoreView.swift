//
//  BestScoreView.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 26/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit

class BestScoreView: UIView
{
    private let fontName: String = "ugly-hero-font"
    
    var bestScoreLabel: UILabel
    var bestMonstersLabel: UILabel
    var bestMetersLabel: UILabel
    
    init(frame: CGRect, gs: GameScene)
    {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 4
        formatter.minimumFractionDigits = 1
        
        let profile = gs.getPlayerProfile()
        
        bestScoreLabel = UILabel(frame: CGRect(x: 0, y: 10,
                                               width: frame.width, height: frame.height/4))
        bestScoreLabel.font = UIFont(name: fontName, size: 30)
        bestScoreLabel.textAlignment = .center
        bestScoreLabel.textColor = UIColor.purple
        bestScoreLabel.text = "BEST SCORE"
        
        bestMonstersLabel = UILabel(frame: CGRect(x: 10, y: bestScoreLabel.frame.height + 10,
                                                  width: frame.width, height: frame.height/4))
        bestMonstersLabel.font = UIFont(name: fontName, size: 20)
        bestMonstersLabel.textAlignment = .left
        bestMonstersLabel.textColor = UIColor.purple
        bestMonstersLabel.text = "- monsters: \(profile.best.Monsters)"

        bestMetersLabel = UILabel(frame: CGRect(x: 10,
                                                y: bestMonstersLabel.frame.height + bestScoreLabel.frame.height,
                                                width: frame.width, height: frame.height/4))
        bestMetersLabel.font = UIFont(name: fontName, size: 20)
        bestMetersLabel.textAlignment = .left
        bestMetersLabel.textColor = UIColor.purple
        bestMetersLabel.text = "- meters: " + formatter.string(for: profile.best.Meters)!
        
        super.init(frame: frame)
        
        let imageView = UIImageView(image: UIImage(named: "best-score.png"))
        imageView.frame.size = frame.size
        imageView.contentMode = .scaleAspectFill
        self.addSubview(imageView)
        
        imageView.addSubview(bestScoreLabel)
        imageView.addSubview(bestMonstersLabel)
        imageView.addSubview(bestMetersLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
