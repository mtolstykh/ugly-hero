//
//  MonsterRoomScene.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 04/03/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameplayKit
import SpriteKit

class MonsterRoomScene: SKScene, SKPhysicsContactDelegate {
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        self.size = view.frame.size
        
        let floor = FloorNode(frameSize: CGSize(
                width: size.width * 2,
                height: size.height
            )
        )
        
        floor.position = CGPoint (
            x: 0,
            y: 0
        )
        
        addChild(floor)
                
        addChild(Grawer(position: CGPoint(x: size.width / 2, y: size.height / 2)))
        addChild(MazzgerHappy(position: CGPoint(x: size.width * 0.75, y: size.height / 2)))
        addChild(MazzgerAngry(position: CGPoint(x: size.width * 0.25, y: size.height / 2)))
        addChild(Frequen(position: CGPoint(x: size.width / 2, y: size.height / 1.2)))
                
        let bgImageQuit = UIImage(named: "quit.png")
        let bgImagePressedQuit = UIImage(named: "quit-pressed.png")
        let quitButton = UIButton()
        quitButton.frame.size = CGSize(width: (bgImageQuit?.size.width)! / 3, height: (bgImageQuit?.size.height)! / 3)
        quitButton.frame.origin = CGPoint(
            x: 15,
            y: 15
        )
        quitButton.setImage(bgImageQuit, for: UIControlState.normal)
        quitButton.setImage(bgImagePressedQuit, for: UIControlState.highlighted)
        quitButton.imageView?.contentMode = .scaleAspectFit
        quitButton.addTarget(self, action: #selector(quit), for: .touchUpInside)

        self.view?.addSubview(quitButton)
    }
    
    func quit()
    {
        self.view?.removeFromSuperview()
    }
    
    override func update(_ currentTime: TimeInterval) {        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
