//
//  KillsLabel.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 17/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class KillsLabel: SKLabelNode
{
    static let killsLabelNodeName = "KillsLabel"
    
    override init()
    {
        super.init()
    }

    init(position: CGPoint, kills: Int)
    {
        super.init(fontNamed: GameScene.fontName)
        self.text = "Monsters: " + String(format: "%03d", kills)
        self.name = KillsLabel.killsLabelNodeName
        self.horizontalAlignmentMode = .left
        self.fontSize = 20
        self.zPosition = 0.2
        self.color = UIColor.white
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
