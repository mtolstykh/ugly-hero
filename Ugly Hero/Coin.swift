//
//  Coin.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 15/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class Coin: SKSpriteNode {
    init(position: CGPoint) {
        let atl = SKTextureAtlas(named: "coin1")
        var textures = [SKTexture]()
        for i in 1...atl.textureNames.count {
            textures.append(SKTexture(imageNamed: "coin-\(i).png"))
        }
        
        let t = SKTexture(imageNamed: "coin")
        super.init(texture: t, color: UIColor.black,
                   size: CGSize(width: t.size().width * 0.05, height: t.size().height * 0.05))
        
        setScale(0.05)
        
        name = "coin"
        self.position = position
        self.zPosition = 0.2
        physicsBody = SKPhysicsBody(circleOfRadius: size.width)
        physicsBody?.mass = 0.1
        physicsBody?.affectedByGravity = false
        physicsBody?.isDynamic = false
        physicsBody?.allowsRotation = false
        physicsBody?.categoryBitMask = PhysicsCategory.Coin
        physicsBody?.collisionBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero
        physicsBody?.contactTestBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero
        
        run(SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: 0.1, resize: true, restore: false)))

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
