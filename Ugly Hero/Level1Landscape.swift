//
//  Level1Landscape.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 24/02/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import Foundation
import SpriteKit

class Level1Landscape: SKNode {
    
    static let Level1Name = "Level1"
    
    let size: CGSize
    let treesCount: Int = 0
    var background = SKSpriteNode()
    var floorHeight: CGFloat
    var treesGenerated = false
    
    static func getInstance(frameSize: CGSize) -> [Level1Landscape] {
        var lvlBgNodes = [Level1Landscape]()
        let xStart = frameSize.width * 2
        
        for i in 0..<SceneConstants.framesPerScene {
            let levelSubFrame = Level1Landscape(
                size: CGSize(
                    width: frameSize.width,
                    height: frameSize.height
                ),
                color: UIColor(
                    red: 0,//CGFloat(i),
                    green: 0.190,//CGFloat(i),
                    blue: 0,//CGFloat(i),
                    alpha: 10//CGFloat(i)
                )
            )
            levelSubFrame.position = CGPoint (
                x: xStart + (frameSize.width) * CGFloat(i),
                y: 0
            )
            levelSubFrame.name = "\(Level1Landscape.Level1Name)-\(i)"
            if i > 0 {
                levelSubFrame.addMonsters()
            }
            
            lvlBgNodes.append(levelSubFrame)
        }
        
        return lvlBgNodes
    }
    
    func go() {
        
    }
    
    convenience init(size: CGSize, color: UIColor) {
        self.init(size: size)
        addBg(color: color)
        addFloor()
        if !treesGenerated {
            generateTrees()
        }
    }
    
    init(size: CGSize) {
        self.size = size
        self.floorHeight =  size.height / 4
        super.init()
        addBg(color: UIColor.black)
        addFloor()
        if !treesGenerated {
            generateTrees()
        }
    }
    
    override init() {
        self.size = CGSize()
        self.floorHeight = 0
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateConfig(count: Int) {
        background.removeAllChildren()
        treesGenerated = false
        
        //if (count >= 3) {
        generateBuildings()
        //} else {
        generateTrees()
        //}
        generateCoins()
        addMonsters()
        runMonsters()
    }
    
    func addMonsters() {
        
        //1. add mazzgers angry & happy
        //2. add grawer
        //3. add frequen
        
        let monsterCount = Int(arc4random_uniform(UInt32(5))) + 2
        
        for _ in 2...monsterCount {
            let monsterId = Int(arc4random_uniform(UInt32(4))) + 1
            let monsterName = getMonsterName(monsterId: monsterId)
            
            let x = CGFloat(arc4random_uniform(UInt32(size.width / 2))) + 1
            let y = CGFloat(arc4random_uniform(UInt32(size.height / 2))) + 1
            let monster = MonsterFactory.generate(instanceOf: monsterName, at: CGPoint(x: x, y: y + floorHeight))
            addChild(monster)
        }
    }
    
    func addBg(color: UIColor) {
        self.background = SKSpriteNode(
            color:  color,
            size: CGSize(width: size.width, height: size.height)
        )
        self.background.position = CGPoint(
            x: 0,
            y: floorHeight
        )
        self.addChild(background)
    }
    
    func addFloor() {
        let floor = SKSpriteNode(
            color: UIColor.brown,
            size: CGSize(width: size.width, height: floorHeight)
        )
        //        floor.size = CGSize(width: size.width, height: size.height / 4)
        floor.position = CGPoint (
            x: 0,
            y: 0
        )
        floor.name = "floor"
        floor.physicsBody = SKPhysicsBody(
            rectangleOf: CGSize(
                width: floor.size.width,
                height: floorHeight / 2))
        
        floor.physicsBody?.affectedByGravity = false
        floor.physicsBody?.isDynamic = false
        floor.physicsBody?.categoryBitMask = PhysicsCategory.Floor
        floor.physicsBody?.collisionBitMask = PhysicsCategory.Hero | PhysicsCategory.Monster
        floor.physicsBody?.contactTestBitMask = PhysicsCategory.Hero | PhysicsCategory.Monster
        self.addChild(floor)
    }
    
    func generateCoins()
    {
        let coinsCount = Int(arc4random_uniform(UInt32(10)))
        var xPos = CGFloat(arc4random_uniform(UInt32(background.size.width - 30)))
        let yPos = CGFloat(arc4random_uniform(UInt32(background.size.height - 100)))

        for _ in 0...coinsCount {
            let coin = Coin(position: CGPoint(
                x: xPos - background.size.width/2,
                y: yPos
            ))
            
            xPos += coin.size.width + 20
            
            background.addChild(coin)
        }
    }
    
    func generateTrees() {
        let treesCount = Int(arc4random_uniform(UInt32(20)))
        for i in 0...treesCount {
            var treeName = "tree-1"
            if i == treesCount - 1 {
                treeName = "tree-2"
            }
            let t = SKSpriteNode(imageNamed: treeName)
            
            t.name = "tree.\(i)"
            t.size = CGSize(width: 30, height: 100)
            
            let xPos = CGFloat(arc4random_uniform(UInt32(background.size.width - 30)))
            let yPos = CGFloat(arc4random_uniform(UInt32(background.size.height - 100)))
            
            t.position = CGPoint(
                x: xPos - background.size.width/2,
                y: yPos
            )
            
            background.addChild(t)
            
            treesGenerated = true
        }
    }
    
    func generateBuildings() {
        let treesCount = Int(arc4random_uniform(UInt32(10)))
        for i in 0...treesCount {
            let treeIndex = Int(arc4random_uniform(UInt32(4))) + 1
            let treeName = "building-\(treeIndex)"
            let t = SKSpriteNode(imageNamed: treeName)
            
            t.name = "building.\(i)"
            t.size = CGSize(width: 30, height: 100)
            
            let xPos = CGFloat(arc4random_uniform(UInt32(background.size.width - 30)))
            let yPos = CGFloat(arc4random_uniform(UInt32(background.size.height - 100)))
            
            t.position = CGPoint(
                x: xPos - background.size.width/2,
                y: yPos
            )
            
            background.addChild(t)
        }
        
    }
    
    func runMonsters() {
        for child in self.children {
            if let monster = child as? MonsterNode {
                let jumpingPower = CGFloat(arc4random_uniform(UInt32(30))) + 40
                let jumpingDuration = CGFloat(arc4random_uniform(UInt32(3))) + 2
                monster.jumping(
                    power: jumpingPower,
                    duration: TimeInterval(jumpingDuration)
                )
            }
        }
        
    }
    
    func getMonsterName(monsterId: Int) -> String
    {
        
        switch monsterId {
        case 1: return "grawer"
        case 2: return "mazzger-angry"
        case 3: return "mazzger-happy"
        case 4: return "frequen"
            //        case 5: return "sheep"
        //        case 6: return "turtle"
        default: return "frequen"
        }
        
    }
    
}
