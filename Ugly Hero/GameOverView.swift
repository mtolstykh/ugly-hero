//
//  GameOverView.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 20/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit

class GameOverView: UIView
{
    private let fontSize: CGFloat = 30.0
    private let fontName: String = "ugly-hero-font"
    private let leftSideOffset: CGFloat = 30.0
    
    let restartButton: UIButton
    let monsterRoomButton: UIButton
    let levelsButton: UIButton
    let heroesRoomButton: UIButton
    let leaderboardsButton: UIButton
    let achievementsButton: UIButton
    let shopButton: UIButton
    
    let totalCoinsLabel: UILabel
    let gameOverLabel: UILabel
    let monstersLabel: UILabel
    let metersLabel: UILabel
    let coinsLabel: UILabel
    
    init(frame: CGRect, gs: GameScene) {
        
        // GAMEOVER
        gameOverLabel = UILabel(
            frame: CGRect(
                x: frame.size.width / 2 - 150,
                y: 10,
                width: 300,
                height: 40
            )
        )
        gameOverLabel.font = UIFont(name: fontName, size: 70)
        gameOverLabel.textAlignment = .center
        gameOverLabel.textColor = UIColor.red
        gameOverLabel.text = "GAME OVER"
        
        // MONSTERS
        monstersLabel = UILabel()
        monstersLabel.frame.origin = CGPoint(
            x: leftSideOffset,
            y: gameOverLabel.frame.origin.y + gameOverLabel.frame.size.height + 10
        )
        monstersLabel.frame.size = CGSize(width: 300, height: 30)
        monstersLabel.font = UIFont(name: fontName, size: fontSize)
        monstersLabel.textAlignment = .left
        monstersLabel.textColor = UIColor.red
        monstersLabel.text = gs.killsLabel.text
        
        // METERS
        metersLabel = UILabel()
        metersLabel.frame.origin = CGPoint(
            x: leftSideOffset,
            y: monstersLabel.frame.origin.y + monstersLabel.frame.size.height + 2
        )
        metersLabel.frame.size = CGSize(width: 300, height: 30)
        metersLabel.font = UIFont(name: fontName, size: fontSize)
        metersLabel.textAlignment = .left
        metersLabel.textColor = UIColor.red
        metersLabel.text = gs.metersLabel.text
        
        // COINS
        coinsLabel = UILabel()
        coinsLabel.frame.origin = CGPoint(
            x: leftSideOffset,
            y: metersLabel.frame.origin.y + metersLabel.frame.size.height + 2
        )
        coinsLabel.frame.size = CGSize(width: 300, height: 30)
        coinsLabel.font = UIFont(name: fontName, size: fontSize)
        coinsLabel.textAlignment = .left
        coinsLabel.textColor = UIColor.red
        coinsLabel.text = gs.coinsLabel.text
        
        // TOTAL COINS
        totalCoinsLabel = UILabel()
        totalCoinsLabel.frame.size = CGSize(width: 50, height: 30)
        totalCoinsLabel.frame.origin = CGPoint(
            x: frame.size.width/2 - totalCoinsLabel.frame.size.width / 2,
            y: frame.size.height - totalCoinsLabel.frame.size.height - 10
        )
        totalCoinsLabel.font = UIFont(name: fontName, size: fontSize)
        totalCoinsLabel.textAlignment = .left
        totalCoinsLabel.adjustsFontSizeToFitWidth = true
        totalCoinsLabel.textColor = UIColor.white
        if let gsView = gs.view {
            if let gvc = gsView.window?.rootViewController as? GameViewController {
                totalCoinsLabel.text = "\(gvc.playerProfile.totalCoins)"
            }
        }
        let coinImageView = GameScene.getCoinImageView()
        coinImageView.frame.origin = CGPoint(
            x: totalCoinsLabel.frame.origin.x - coinImageView.frame.size.width,
            y: totalCoinsLabel.frame.origin.y + coinImageView.frame.size.height / 2
        )
        
        // LEVELS ROOM
        let levelsRoomBgImage = UIImage(named: "levels.png")
        levelsButton = UIButton()
        levelsButton.frame.size = CGSize(width: (levelsRoomBgImage?.size.width)! / 2,
                                         height: (levelsRoomBgImage?.size.height)! / 2)
        levelsButton.frame.origin = CGPoint(
            x: leftSideOffset,
            y: frame.size.height - levelsButton.frame.size.height - 10
        )
        levelsButton.setImage(levelsRoomBgImage, for: UIControlState.normal)
        levelsButton.imageView?.contentMode = .scaleAspectFit
        levelsButton.addTarget(gs, action: #selector(GameScene.levelsRoom), for: .touchUpInside)
        
        // MONSTERS ROOM
        let monsterRoomBgImage = UIImage(named: "monsters-room.png")
        monsterRoomButton = UIButton()
        monsterRoomButton.frame.size = CGSize(width: (monsterRoomBgImage?.size.width)! / 2,
                                              height: (monsterRoomBgImage?.size.height)! / 2)
        monsterRoomButton.frame.origin = CGPoint(
            x: leftSideOffset,
            y: levelsButton.frame.origin.y - monsterRoomButton.frame.size.height - 10
        )
        monsterRoomButton.setImage(monsterRoomBgImage, for: UIControlState.normal)
        monsterRoomButton.imageView?.contentMode = .scaleAspectFit
        monsterRoomButton.addTarget(gs, action: #selector(GameScene.monsterRoom), for: .touchUpInside)
        
        // HEROES ROOM
        let heroesRoomBgImage = UIImage(named: "heroes-room.png")
        heroesRoomButton = UIButton()
        heroesRoomButton.frame.size = CGSize(width: (heroesRoomBgImage?.size.width)! / 2,
                                             height: (heroesRoomBgImage?.size.height)! / 2)
        heroesRoomButton.frame.origin = CGPoint(
            x: monsterRoomButton.frame.origin.x + monsterRoomButton.frame.size.width - heroesRoomButton.frame.size.width,
            y: frame.size.height - heroesRoomButton.frame.size.height - 10
        )
        heroesRoomButton.setImage(heroesRoomBgImage, for: UIControlState.normal)
        heroesRoomButton.imageView?.contentMode = .scaleAspectFit
        heroesRoomButton.addTarget(gs, action: #selector(GameScene.heroesRoom), for: .touchUpInside)
        
        // LEADERBOARDS
        let leaderboardsBgImage = UIImage(named: "leaderboards.png")
        leaderboardsButton = UIButton()
        leaderboardsButton.frame.size = CGSize(width: (leaderboardsBgImage?.size.width)! / 2,
                                               height: (leaderboardsBgImage?.size.height)! / 2)
        leaderboardsButton.frame.origin = CGPoint(
            x: frame.size.width - leaderboardsButton.frame.size.width - 15,
            y: gameOverLabel.frame.origin.y + gameOverLabel.frame.size.height + 10
        )
        leaderboardsButton.setImage(leaderboardsBgImage, for: UIControlState.normal)
        leaderboardsButton.imageView?.contentMode = .scaleAspectFit
        leaderboardsButton.addTarget(gs.view?.window?.rootViewController, action: #selector(GameViewController.showLeaderboard), for: .touchUpInside)
        
        // ACHIEVEMENTS
        let achievementsBgImage = UIImage(named: "achievements.png")
        achievementsButton = UIButton()
        achievementsButton.frame.size = CGSize(width: (achievementsBgImage?.size.width)! / 2,
                                               height: (achievementsBgImage?.size.height)! / 2)
        achievementsButton.frame.origin = CGPoint(
            x: frame.size.width - achievementsButton.frame.size.width - 15,
            y: leaderboardsButton.frame.origin.y + leaderboardsButton.frame.size.height + 10
        )
        achievementsButton.setImage(achievementsBgImage, for: UIControlState.normal)
        achievementsButton.imageView?.contentMode = .scaleAspectFit
        achievementsButton.addTarget(gs, action: #selector(GameScene.achievementsRoom), for: .touchUpInside)
        
        // RESTART
        let bgImage = UIImage(named: "retry.png")
        let bgImagePressed = UIImage(named: "retry-pressed.png")
        restartButton = UIButton()
        restartButton.frame.size = CGSize(width: (bgImage?.size.width)! / 2, height: (bgImage?.size.height)! / 2)
        restartButton.frame.origin = CGPoint(
            x: frame.size.width - restartButton.frame.size.width - 15,
            y: frame.size.height - restartButton.frame.size.height - 15
        )
        restartButton.setImage(bgImage, for: UIControlState.normal)
        restartButton.setImage(bgImagePressed, for: UIControlState.highlighted)
        restartButton.imageView?.contentMode = .scaleAspectFit
        restartButton.addTarget(gs, action: #selector(GameScene.replay), for: .touchUpInside)
        
        // RESTART
        let bgImagePause = UIImage(named: "Bucket.png")
        shopButton = UIButton()
        shopButton.frame.size = CGSize(width: (bgImagePause?.size.width)! / 30, height: (bgImagePause?.size.height)! / 30)
        shopButton.frame.origin = CGPoint(
            x: achievementsButton.frame.origin.x + achievementsButton.frame.size.width - shopButton.frame.size.width,
            y: achievementsButton.frame.origin.y + achievementsButton.frame.size.height + 10
        )
        shopButton.setImage(bgImagePause, for: UIControlState.normal)
        shopButton.imageView?.contentMode = .scaleAspectFit
        shopButton.addTarget(gs, action: #selector(GameScene.shop), for: .touchUpInside)
        
        // BEST SCORE
        let bestScoreView = BestScoreView(
            frame: CGRect(
                x: frame.width / 2.7,
                y: frame.height / 2.7,
                width: frame.width / 3.5,
                height: frame.width / 3.5
            ),
            gs: gs
        )
        
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.darkGray
        self.isOpaque = false
        self.alpha = 0.9
        
        self.addSubview(bestScoreView)
        self.addSubview(coinImageView)
        self.addSubview(gameOverLabel)
        self.addSubview(monstersLabel)
        self.addSubview(metersLabel)
        self.addSubview(coinsLabel)
        self.addSubview(monsterRoomButton)
        self.addSubview(levelsButton)
        self.addSubview(heroesRoomButton)
        self.addSubview(restartButton)
        self.addSubview(leaderboardsButton)
        self.addSubview(achievementsButton)
        self.addSubview(totalCoinsLabel)
        self.addSubview(shopButton)
        
        let profile = gs.getPlayerProfile()
        if profile.helpers.contains(Helpers.Life) {
            let lifeImage = UIImage(named: "life.png")
            let lifeButton = UIButton()
            lifeButton.frame.size = CGSize(width: (lifeImage?.size.width)! * 0.75, height: (lifeImage?.size.height)! * 0.75)
            lifeButton.setImage(lifeImage, for: UIControlState.normal)
            lifeButton.frame.origin = CGPoint(
                x: achievementsButton.frame.origin.x,
                y: achievementsButton.frame.origin.y + achievementsButton.frame.size.height + 10
            )
            lifeButton.addTarget(gs, action: #selector(GameScene.useLife), for: .touchUpInside)
            self.addSubview(lifeButton)
        }
        
        if let gvc = gs.view?.window?.rootViewController as? GameViewController {
            if gvc.playerProfile.adBlock && gs.interstitial.isReady {
                gs.interstitial.present(fromRootViewController: gvc)
            } else {
                print("Ad wasn't ready")
            }
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
