//
//  GamePauseView.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 20/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit

class GamePauseView: UIView
{
    private let fontName: String = "ugly-hero-font"
    private let fontSize: CGFloat = 30.0
    
    let restartButton: UIButton
    let quitButton: UIButton
    let quitShopButton: UIButton
    let resumeButton: UIButton
    
    init(frame: CGRect, gs: GameScene) {
        
        // RESTART
        restartButton = UIButton()
        let bgImage = UIImage(named: "retry.png")
        let bgImagePressed = UIImage(named: "retry-pressed.png")
        restartButton.frame.size = CGSize(width: (bgImage?.size.width)! / 2, height: (bgImage?.size.height)! / 2)
        restartButton.frame.origin = CGPoint(x: 10, y: frame.size.height - restartButton.frame.size.height - 10)
        restartButton.setImage(bgImage, for: UIControlState.normal)
        restartButton.setImage(bgImagePressed, for: UIControlState.highlighted)
        restartButton.imageView?.contentMode = .scaleAspectFit
        restartButton.addTarget(gs, action: #selector(GameScene.replay), for: .touchUpInside)
        
        // QUIT
        let bgImageQuit = UIImage(named: "quit.png")
        let bgImagePressedQuit = UIImage(named: "quit-pressed.png")
        quitButton = UIButton()
        quitButton.frame.size = CGSize(width: (bgImageQuit?.size.width)! / 2, height: (bgImageQuit?.size.height)! / 2)
        quitButton.frame.origin = CGPoint(
            x: frame.size.width / 2 - quitButton.frame.size.width / 2,
            y: frame.size.height - quitButton.frame.size.height - 10
        )
        quitButton.setImage(bgImageQuit, for: UIControlState.normal)
        quitButton.setImage(bgImagePressedQuit, for: UIControlState.highlighted)
        quitButton.imageView?.contentMode = .scaleAspectFit
        quitButton.addTarget(gs, action: #selector(GameScene.quit), for: .touchUpInside)
        
        // QUIT SHOP
        quitShopButton = UIButton()
        quitShopButton.isHidden = true
        quitShopButton.isEnabled = false
        quitShopButton.frame.size = CGSize(width: (bgImageQuit?.size.width)! / 2, height: (bgImageQuit?.size.height)! / 2)
        quitShopButton.frame.origin = CGPoint(
            x: frame.size.width / 2 - quitShopButton.frame.size.width / 2,
            y: frame.size.height - quitShopButton.frame.size.height - 5
        )
        quitShopButton.setImage(bgImageQuit, for: UIControlState.normal)
        quitShopButton.setImage(bgImagePressedQuit, for: UIControlState.highlighted)
        quitShopButton.imageView?.contentMode = .scaleAspectFit
        quitShopButton.addTarget(gs, action: #selector(GameScene.quitShop), for: .touchUpInside)
        
        // RESUME
        resumeButton = UIButton()
        let bgImageResume = UIImage(named: "resume.png")
        let bgImagePressedResume = UIImage(named: "resume-pressed.png")
        resumeButton.frame.size = CGSize(width: (bgImageResume?.size.width)! / 2, height: (bgImageResume?.size.height)! / 2)
        resumeButton.frame.origin = CGPoint(
            x: frame.size.width - resumeButton.frame.size.width - 10,
            y: frame.size.height - resumeButton.frame.size.height - 10
        )
        resumeButton.setImage(bgImageResume, for: UIControlState.normal)
        resumeButton.setImage(bgImagePressedResume, for: UIControlState.highlighted)
        resumeButton.imageView?.contentMode = .scaleAspectFit
        resumeButton.addTarget(gs, action: #selector(GameScene.resume), for: .touchUpInside)

        // BOMB
        let bombImage = UIImage(named: "bomb.png")
        let bombButton = UIButton()
        bombButton.frame.size = CGSize(
            width: frame.size.width / 10,
            height: frame.size.width / 10
        )        
        bombButton.setImage(bombImage, for: UIControlState.normal)
        bombButton.backgroundColor = UIColor.white
        bombButton.frame.origin = CGPoint(
            x: frame.size.width * 0.25 - bombButton.frame.size.width / 2,
            y: 5
        )
        bombButton.addTarget(gs, action: #selector(GameScene.buyBomb), for: .touchUpInside)
        let bombCostLabel = GamePauseView.getCoinLabel(text: "100")
        bombCostLabel.frame.origin = CGPoint(
            x: bombButton.frame.origin.x ,
            y: bombButton.frame.origin.y + bombButton.frame.size.height
        )
        let bombCoinImageView = GameScene.getCoinImageView()
        bombCoinImageView.frame.origin = CGPoint(
            x: bombCostLabel.frame.origin.x - bombCoinImageView.frame.size.width,
            y: bombCostLabel.frame.origin.y + bombCoinImageView.frame.size.height / 2
        )

        
        // CAPSULA
        let capsulaImage = UIImage(named: "capsula.png")
        let capsulaButton = UIButton()
        capsulaButton.frame.size = CGSize(
            width: frame.size.width / 10,
            height: frame.size.width / 10
        )
        capsulaButton.backgroundColor = UIColor.white
        capsulaButton.setImage(capsulaImage, for: UIControlState.normal)
        capsulaButton.frame.origin = CGPoint(
            x: frame.size.width / 2 - capsulaButton.frame.size.width / 2,
            y: 5
        )
        capsulaButton.addTarget(gs, action: #selector(GameScene.buyCapsula), for: .touchUpInside)
        let capsulaCostLabel = GamePauseView.getCoinLabel(text: "300")
        capsulaCostLabel.frame.origin = CGPoint(
            x: capsulaButton.frame.origin.x ,
            y: capsulaButton.frame.origin.y + capsulaButton.frame.size.height
        )
        let capsulaCoinImageView = GameScene.getCoinImageView()
        capsulaCoinImageView.frame.origin = CGPoint(
            x: capsulaCostLabel.frame.origin.x - capsulaCoinImageView.frame.size.width,
            y: capsulaCostLabel.frame.origin.y + capsulaCoinImageView.frame.size.height / 2
        )
        
        // LIFE
        let lifeImage = UIImage(named: "life.png")
        let lifeButton = UIButton()
        lifeButton.frame.size = CGSize(
            width: frame.size.width / 10,
            height: frame.size.width / 10
        )
        lifeButton.setImage(lifeImage, for: UIControlState.normal)
        lifeButton.backgroundColor = UIColor.white
        lifeButton.frame.origin = CGPoint(
            x: frame.size.width * 0.75,
            y: 5
        )
        lifeButton.addTarget(gs, action: #selector(GameScene.buyLife), for: .touchUpInside)
        let lifeCostLabel = GamePauseView.getCoinLabel(text: "200")
        let lifeCoinImageView = GameScene.getCoinImageView()
        lifeCoinImageView.frame.origin = CGPoint(
            x: lifeButton.frame.origin.x,
            y: lifeButton.frame.origin.y + lifeButton.frame.size.height + 8
        )
        lifeCostLabel.frame.origin = CGPoint(
            x: lifeButton.frame.origin.x + lifeCoinImageView.frame.size.width,
            y: lifeCoinImageView.frame.origin.y
        )
        
        super.init(frame: frame)
        
        self.isOpaque = false
        self.alpha = 0.9
        self.backgroundColor = UIColor.darkGray
        
        // PURCHASING
        let purchasingView = PurchasingView(frame: frame, gs: gs)
        purchasingView.frame.origin = CGPoint(x: 0, y: frame.size.height / 3)
        
        self.addSubview(purchasingView)
        self.addSubview(restartButton)
        self.addSubview(quitButton)
        self.addSubview(quitShopButton)
        self.addSubview(resumeButton)
        self.addSubview(bombButton)
        self.addSubview(capsulaButton)
        self.addSubview(lifeButton)
        self.addSubview(bombCostLabel)
        self.addSubview(bombCoinImageView)
        self.addSubview(capsulaCostLabel)
        self.addSubview(capsulaCoinImageView)
        self.addSubview(lifeCostLabel)
        self.addSubview(lifeCoinImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static func getCoinLabel(text: String) -> UILabel {
        let label = UILabel()
        label.frame.size = CGSize(width: 50, height: 30)
        
        label.font = UIFont(name: "ugly-hero-font", size: 30)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.text = text
        return label
    }
}
