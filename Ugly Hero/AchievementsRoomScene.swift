//
//  AchievementsRoomScene.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 27/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameKit


class AchievementsRoomScene: SKScene
{
    static let achievements = [
        AchievementsManager.monsters_frequen:       ["5-frequen",       "10-frequen",       "15-frequen"        ],
        AchievementsManager.monsters_grawer:        ["5-grawer",        "10-grawer",        "15-grawer"         ],
        AchievementsManager.monsters_mazzger_angry: ["5-mazzger-angry", "10-mazzger-angry", "15-mazzger-angry"  ],
        AchievementsManager.monsters_mazzger_happy: ["5-mazzger-happy", "10-mazzger-happy", "15-mazzger-happy"  ],
        AchievementsManager.meters:                 ["100-meters-ach",  "300-meters-ach",   "500-meters-ach"    ],
        AchievementsManager.monsters:               ["20-monsters-ach", "30-monsters-ach",  "50-monsters-ach"   ],
        AchievementsManager.monsters_fly:           ["3-fly-kills",     "5-fly-kills",      "7-fly-kills"       ],
        AchievementsManager.monsters_jump:          ["10-kills-jump",   "20-kills-jump",    "30-kills-jump"     ]
    ]
    
    
    
    override func didMove(to view: SKView)
    {
        self.view?.addSubview(AchievementsView(frame: (self.view?.frame)!, achievements: AchievementsRoomScene.achievements))
    }        
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
