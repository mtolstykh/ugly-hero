//
//  AchievementsView.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 24/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import GameKit

class AchievementsView: UIView
{
    let offset: CGFloat = 8.0
    var offsetX: CGFloat = 0.0
    var offsetY: CGFloat = 0.0
    var rowCount = 0
    
    init(frame: CGRect, achievements: Dictionary<String, [String]>) {
        super.init(frame: frame)
        
        let btnWidth = (frame.width) / 6 - offset
        let btnHeight = (frame.height) / 6 - offset
        
        self.backgroundColor = UIColor.white
        
        for ach in achievements
        {
            for imageName in ach.value {
                let bgImage = UIImage(named: "\(imageName).png")
                let btn = UIButton()
                btn.accessibilityIdentifier = imageName
                btn.frame.size = CGSize(
                    width: btnWidth,
                    height: btnHeight
                )
                btn.isEnabled = false
                btn.frame.origin = CGPoint(x: offset/2 + offsetX, y: offset + offsetY)
                btn.setImage(bgImage, for: UIControlState.normal)
                self.addSubview(btn)
                
                rowCount += 1
                offsetX += btnWidth + offset
                
                if rowCount == 6 {
                    offsetY += btnHeight + offset
                    offsetX = 0
                    rowCount = 0
                }
            }
        }
        
        let bgImageQuit = UIImage(named: "quit.png")
        let bgImagePressedQuit = UIImage(named: "quit-pressed.png")
        let quitButton = UIButton()
        quitButton.frame.size = CGSize(width: (bgImageQuit?.size.width)! / 3, height: (bgImageQuit?.size.height)! / 3)
        quitButton.frame.origin = CGPoint(
            x: (frame.width) - quitButton.frame.size.width - 15,
            y: (frame.height) - quitButton.frame.size.height - 15
        )
        quitButton.setImage(bgImageQuit, for: UIControlState.normal)
        quitButton.setImage(bgImagePressedQuit, for: UIControlState.highlighted)
        quitButton.imageView?.contentMode = .scaleAspectFit
        quitButton.addTarget(self, action: #selector(quit), for: .touchUpInside)
        
        self.addSubview(quitButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func quit()
    {
        if let _ = self.superview as? GameOverView {
            self.removeFromSuperview()
        }
        else {
            self.superview?.removeFromSuperview()
        }
    }
}
