
//
//  Bomb.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 25/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class Bomb: SKSpriteNode
{
    init() {
        
        let t = SKTexture(imageNamed: "bomb.png")
        super.init(texture: t, color: UIColor.black,
                   size: CGSize(width: t.size().width / 2, height: t.size().height / 2))
                
        physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2)
        physicsBody?.mass = 0.000001
        physicsBody?.affectedByGravity = true
        physicsBody?.isDynamic = true
        physicsBody?.allowsRotation = false
        physicsBody?.categoryBitMask = PhysicsCategory.Bomb
        physicsBody?.collisionBitMask = PhysicsCategory.Floor
        physicsBody?.contactTestBitMask = PhysicsCategory.Floor

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
