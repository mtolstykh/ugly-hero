//
//  PlayerProfile.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/15/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import Foundation
import GameKit

class PlayerProfile: NSObject, NSCoding
{
    let lbDistance = "com.braincrisys.UglyHero.distanse"
    let lbMonsters = "com.braincrisys.UglyHero.monsters"
    
    var last: GameRun = GameRun()
    var best: GameRun = GameRun()
    var totalCoins: Int = 0
    var helpers: [String] = []
    var adBlock: Bool = true;
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.helpers = aDecoder.decodeObject(forKey: "helpers") as! [String]
        self.totalCoins = aDecoder.decodeInteger(forKey: "totalCoins")
        self.last = aDecoder.decodeObject(forKey: "lastRun") as! GameRun
        self.best = aDecoder.decodeObject(forKey: "bestRun") as! GameRun
        self.adBlock = aDecoder.decodeBool(forKey: "adBlock") 
    }
    
    func update(gameRun: GameRun)
    {
        last = gameRun
        
        if best.Coins <= last.Coins &&
            best.Meters <= last.Meters &&
            best.Monsters <= last.Monsters
        {
            best = gameRun
            reportLB(LEADERBOARD_ID: lbDistance, value: Int(best.Meters))
            reportLB(LEADERBOARD_ID: lbMonsters, value: best.Monsters)
        }
        
        self.totalCoins += last.Coins
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(helpers, forKey: "helpers")
        aCoder.encode(totalCoins, forKey: "totalCoins")
        aCoder.encode(last, forKey: "lastRun")
        aCoder.encode(best, forKey: "bestRun")
        aCoder.encode(adBlock, forKey: "adBlock")
    }
    
    func reportLB(LEADERBOARD_ID: String, value: Int) {
        let bestScoreInt = GKScore(leaderboardIdentifier: LEADERBOARD_ID)
        bestScoreInt.value = Int64(value)
        GKScore.report([bestScoreInt]) { (error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print("Best Score submitted to your Leaderboard!")
            }
        }

    }
    
    
}
