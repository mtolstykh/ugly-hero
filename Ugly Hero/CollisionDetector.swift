//
//  CollisionDetector
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 26/02/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit
import UIKit

class CollisionDetector: NSObject, SKPhysicsContactDelegate {
    
    var gameScene: GameScene?
    
    var flyKill = 0
    var jumpKill = 0
    var grawerKill = 0
    var mazzgerHKill = 0
    var mazzgerAKill = 0
    var frequenKill = 0
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        let cA = contact.bodyA.categoryBitMask
        let cB = contact.bodyB.categoryBitMask
        
        if let nodeA = contact.bodyA.node as? SKSpriteNode,
            let nodeB = contact.bodyB.node as? SKSpriteNode
        {
            if (cA == PhysicsCategory.Bullet && cB == PhysicsCategory.Monster) ||
                (cB == PhysicsCategory.Bullet && cA == PhysicsCategory.Monster)
            {
                //kill monster
                nodeA.removeFromParent()
                nodeB.removeFromParent()
                if let _ = nodeB as? MonsterNode { killProcessing(node: nodeB, fly: isFly(node: nodeB), jump: isJump(node: nodeB)) }
                if let _ = nodeA as? MonsterNode { killProcessing(node: nodeA, fly: isFly(node: nodeA), jump: isJump(node: nodeA)) }
                kill()
            }
            
            if (cA == PhysicsCategory.Hero && cB == PhysicsCategory.Monster)
            {
                if let hero = nodeA as? HeroNode {
                    if hero.capsula {
                        killProcessing(node: nodeB, fly: isFly(node: nodeB), jump: isJump(node: nodeB))
                        nodeB.removeFromParent()
                        kill()
                        return
                    }
                }
                if (contact.contactNormal.dx == 0.0 &&
                    contact.contactNormal.dy == -1.0)
                {
                    killProcessing(node: nodeB, fly: isFly(node: nodeB), jump: isJump(node: nodeB))
                    nodeB.removeFromParent()
                    kill()
                } else {
                    nodeB.removeFromParent()
                    gameOver()
                }
            }
            else if (cB == PhysicsCategory.Hero && cA == PhysicsCategory.Monster)
            {
                if let hero = nodeB as? HeroNode {
                    if hero.capsula {
                        killProcessing(node: nodeA, fly: isFly(node: nodeA), jump: isJump(node: nodeA))
                        nodeA.removeFromParent()
                        kill()
                        return
                    }
                }
                
                if contact.contactNormal.dx == 0.0 &&
                    contact.contactNormal.dy == 1.0 {
                    killProcessing(node: nodeA, fly: isFly(node: nodeA), jump: isJump(node: nodeA))
                    nodeA.removeFromParent()
                    kill()
                } else {
                    nodeA.removeFromParent()
                    gameOver()
                }
            }
            
            if (cA == PhysicsCategory.Hero && cB == PhysicsCategory.Coin)
            {
                nodeA.run(SKAction.playSoundFileNamed("coin-sound.m4a", waitForCompletion: true))
                coinCatch()
                nodeB.removeFromParent()
            }
            else if (cB == PhysicsCategory.Hero && cA == PhysicsCategory.Coin)
            {
                nodeB.run(SKAction.playSoundFileNamed("coin-sound.m4a", waitForCompletion: true))
                coinCatch()
                nodeA.removeFromParent()
            }
            
            if (cA == PhysicsCategory.Bomb && cB == PhysicsCategory.Floor) ||
                (cB == PhysicsCategory.Bomb && cA == PhysicsCategory.Floor)
            {
                //boom sound
                if let gs = gameScene {
                    for level in gs.children {
                        if let lvl = level as? Level1Landscape {
                            for monsters in lvl.children {
                                if let m = monsters as? MonsterNode {
                                    m.removeFromParent()
                                    kill()
                                }
                            }
                        }
                    }
                }
                if let b = nodeA as? Bomb {
                    b.removeFromParent()
                }
                if let b = nodeB as? Bomb {
                    b.removeFromParent()
                }
            }
        }
    }
    
    func gameOver()
    {
        if let gs = gameScene
        {
            gs.state = GameState.GameOver
            if let gsView = gs.view {
                if let gameController = gsView.window?.rootViewController as? GameViewController {
                    let gameRun = GameRun(
                        monsters: gs.kills,
                        meters: gs.meters,
                        coins: gs.coins
                    )
                    
                    gameRun.MonstersFly          = flyKill
                    gameRun.MonstersJump         = jumpKill
                    gameRun.MonstersFrequen      = frequenKill
                    gameRun.MonstersMazzgerAngry = mazzgerAKill
                    gameRun.MonstersMazzgerHappy = mazzgerHKill
                    gameRun.MonstersGrawer       = grawerKill
                    
                    gameController.playerProfile.update(
                        gameRun: gameRun
                    )
                    
                    let width = gs.view?.frame.size.width
                    let height = gs.view?.frame.size.height
                    
                    let gameOverView = GameOverView(
                        frame: CGRect(
                            x: 0,
                            y: 0,
                            width: width!,
                            height: height!
                        ),
                        gs: gs
                    )
                    gs.view?.addSubview(gameOverView)
                    
                    gs.killsLabel.removeFromParent()
                    gs.metersLabel.removeFromParent()
                    gs.coinsLabel.removeFromParent()
                    
                    GameScene.gameOnPause = true
                    gs.view?.isPaused = true
                    gs.isPaused = true
                    
                    if gameController.achievementManager.process(gameRun: gameRun) {
                        let achView = AchievementsView(frame: (gs.view?.frame)!, achievements: AchievementsRoomScene.achievements)
                        
                        gameOverView.addSubview(achView)
                        
                        var achs = [GKAchievement]()
                        
//                        UIView.animate(withDuration: 1) {
//                            
                            for achName in gameController.achievementManager.completed {
                                
                                let achievementID = self.getAchievementID(achievmentName: achName)
                                
                                print("Complete achievement \(achievementID)")
                                let a = GKAchievement(identifier: achievementID)
                                a.showsCompletionBanner = true
                                a.percentComplete = 100.0
                                achs.append(a)
//
//                                let achImageName = self.getAchievementImageName(achievmentName: achName)
//                                let iv = UIImageView(image: UIImage(named: "\(achImageName).png"))
//                                var svBtn = UIButton()
//                                for sv in achView.subviews {
//                                    if sv.accessibilityIdentifier == achImageName {
//                                        iv.frame.origin = sv.frame.origin
//                                        if let svb = sv as? UIButton {
//                                            svBtn = svb
//                                        }
//                                    }
//                                }
//                                achView.addSubview(iv)
//                                
//                                UIView.animate(withDuration: 1, animations: {
//                                    iv.frame.size = CGSize(width: iv.frame.size.width + 10, height: iv.frame.size.height + 10)
//                                }) { completed in
//                                    svBtn.isEnabled = true
//                                    
//                                    iv.removeFromSuperview()
//                                }
//                                
                            }
//                        }
                        
                        GKAchievement.report(achs) { error in
                            if (error != nil)
                            {
                                NSLog("Error in reporting achievements: \(error!)");
                            } else {
                                for _ in achs {
                                    gameController.playerProfile.totalCoins += Int(arc4random_uniform(UInt32(100))) + 100
                                }
                            }
                            
                        }
                        
                        gameController.achievementManager.completed.removeAll()
                    }
                }
            }
        }
    }
    
    func getAchievementImageName(achievmentName: String) -> String
    {
        let achieveId = getAchivementId(achievmentName: achievmentName)
        let indexValue = getIndex(achievmentName: achievmentName)
        return (AchievementsRoomScene.achievements[achieveId]?[Int(indexValue)!])!
    }
    
    func getAchievementID(achievmentName: String) -> String
    {
        let achieveId = getAchivementId(achievmentName: achievmentName)
        let indexValue = getIndex(achievmentName: achievmentName)
        let achImageName = AchievementsRoomScene.achievements[achieveId]?[Int(indexValue)!]
        print("Achievement image \(achImageName!).png")
        let achieveVal = AchievementsManager.achievements[achieveId]?[Int(indexValue)!]
        if let intyVal = achieveVal as? Int {
            return "\(achieveId).\(intyVal)"
        } else if let floatyVal = achieveVal as? Float {
            return "\(achieveId).\(Int(floatyVal))"
        }
        
        return ""
    }
    
    func getIndex(achievmentName: String) -> String
    {
        let split = achievmentName.components(separatedBy: ".")
        let indexValue = split.last
        return indexValue!
    }
    
    func getAchivementId(achievmentName: String) -> String
    {
        let split = achievmentName.components(separatedBy: ".")
        var achParts = split.dropLast()
        var achieveId = split.first
        achParts = achParts.dropFirst()
        
        achParts.forEach { part in
            achieveId?.append("." + part)
        }
        
        return achieveId!
    }
    
    func kill()
    {
        if let gs = gameScene
        {
            gs.hero?.run(SKAction.playSoundFileNamed("kill2.m4a", waitForCompletion: true))
            gs.kills += 1
            gs.killsLabel.text = "Monsters: " + String(format: "%03d", gs.kills)
        }
    }
    
    func coinCatch()
    {
        if let gs = gameScene
        {
            gs.coins += 1
            gs.coinsLabel.text = "Coins: " + String(format: "%03d", gs.coins)
            
        }
    }
    
    func isFly(node: SKSpriteNode) -> Bool
    {
        if let gs = gameScene
        {
            if node.frame.origin.y > gs.lvlBgNodes[0].floorHeight + 1.0 &&
                (gs.hero?.frame.origin.y)! > gs.lvlBgNodes[0].floorHeight + 1.0 {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func isJump(node: SKSpriteNode) -> Bool
    {
        if let gs = gameScene
        {
            if node.frame.origin.y < gs.lvlBgNodes[0].floorHeight + 1.0 &&
                (gs.hero?.frame.origin.y)! > gs.lvlBgNodes[0].floorHeight + 1.0 {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func killProcessing(node: SKSpriteNode, fly: Bool, jump: Bool)
    {
        if let _ = node as? Grawer       { grawerKill   += 1 }
        if let _ = node as? MazzgerAngry { mazzgerAKill += 1 }
        if let _ = node as? MazzgerHappy { mazzgerHKill += 1 }
        if let _ = node as? Frequen      { frequenKill  += 1 }
        if fly                           { flyKill      += 1 }
        if jump                          { jumpKill     += 1 }
    }
    
    func reportHiddenAchievements(gc: GameViewController, gameRun: GameRun, completionHandler: ((Error?) -> Swift.Void)? = nil)
    {
        if (gc.achievementManager.process(gameRun: gameRun))
        {
        }
        
    }
    
}
