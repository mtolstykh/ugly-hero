//
//  GameViewController.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 19/02/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GameKit
import StoreKit

struct Scenes {
    static let Monster = "MonsterRoomScene"
    static let Game = "GameScene"
}

class GameViewController: UIViewController, GKGameCenterControllerDelegate {
    
    var playerProfile = PlayerProfile()
    var purchasingManager: PurchasingManager?
    let achievementManager = AchievementsManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(saveGame),
                                               name: NSNotification.Name.UIApplicationWillTerminate,
                                               object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(saveGame),
                                               name: NSNotification.Name.UIApplicationWillResignActive,
                                               object:nil)
        
        purchasingManager = PurchasingManager(rootViewController: self)
        purchasingManager?.restorePurchases()
        
        authenticatePlayer()
        loadGame()
        
        if let view = self.view as! SKView? {
            if let scene = SKScene(fileNamed: Scenes.Game) {
                scene.scaleMode = .aspectFill
                
                for i in 0..<registeredProducts.count {
                    let regProd = registeredProducts[i]
                    purchasingManager?.getInfo(purchase: regProd)
                }
                
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            view.showsFPS = false
            view.showsNodeCount = false
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func authenticatePlayer()
    {
        let localPlayer = GKLocalPlayer.localPlayer()
        localPlayer.authenticateHandler = {
            (view , error) in
            
            if view != nil {
                self.present(view!, animated: true, completion: nil)
            }
            
//            GKAchievement.resetAchievements(completionHandler: {
//                error in
//                if error != nil {
//                    print("Reset all achievements: \(String(describing: error?.localizedDescription))")
//                }
//            })            
            
            GKAchievement.loadAchievements(completionHandler: { achievements, error in
                if achievements != nil {
                    for a in achievements! {
                        print("achievement \(a.identifier!)")
                    }
                }
                
                if error != nil {
                    print("Achievements error \(String(describing: error?.localizedDescription))")
                }
            })
        }
    }
    
    func showLeaderboard()
    {
        let vc = self.view?.window?.rootViewController
        let gc = GKGameCenterViewController()
        gc.gameCenterDelegate = self
        vc?.present(gc, animated: true, completion: nil)
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController)
    {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    func saveGame(notification: NSNotification)
    {
        let saveData = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: saveData)
        
        archiver.encode(playerProfile.last, forKey: "lastRun")
        archiver.encode(playerProfile.best, forKey: "bestRun")
        archiver.encode(playerProfile.totalCoins, forKey: "totalCoins")
        archiver.encode(playerProfile.helpers, forKey: "helpers")
        archiver.encode(playerProfile.adBlock, forKey: "adBlock")
        archiver.finishEncoding()
        
        if let saveLocation = saveFileLocation() {
            _ = saveData.write(to: saveLocation, atomically: true)
        }
        
    }
    
    func loadGame()
    {
        if let saveLocation = saveFileLocation() {
            if let saveData = try? Data(contentsOf: saveLocation)
            {
                let unarchiver = NSKeyedUnarchiver(forReadingWith: saveData)
                if let lastRun = unarchiver.decodeObject(forKey: "lastRun") as? GameRun {
                    playerProfile.last = lastRun
                }
                if let bestRun = unarchiver.decodeObject(forKey: "bestRun") as? GameRun {
                    playerProfile.best = bestRun
                }
                playerProfile.totalCoins = unarchiver.decodeInteger(forKey: "totalCoins")
                if let helpers = unarchiver.decodeObject(forKey: "helpers") as? [String] {
                    playerProfile.helpers = helpers
                }
                playerProfile.adBlock = unarchiver.decodeBool(forKey: "adBlock")
                unarchiver.finishDecoding()
            }
        }
    }
    
    func saveFileLocation() -> URL?
    {
        var path: URL?
        do {
            let fileManager = FileManager()
            let gameSaveDirectory = try fileManager.url(
                for: .applicationSupportDirectory, in: .userDomainMask,
                appropriateFor: nil, create: true)
            
            path = gameSaveDirectory.appendingPathComponent("UglyHero")
            
            if let p = path {
                try fileManager.createDirectory(at: p, withIntermediateDirectories: true, attributes: nil)
            }
            
            path?.appendPathComponent("UH-\(Date().timeIntervalSince1970.binade).savegame" )
            
        } catch {
            
        }
        return path
    }
}
