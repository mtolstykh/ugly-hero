//
//  CoinsLabel.swift
//  Ugly Hero
//
//  Created by Maxim Tolstykh on 17/04/2017.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class CoinsLabel: SKLabelNode
{
    static let coinsLabelNodeName = "CoinsLabel"
    
    override init()
    {
        super.init()
    }
    
    init(position: CGPoint, coins: Int)
    {
        super.init(fontNamed: GameScene.fontName)
        self.text = "Coins: " + String(format: "%03d", coins)
        self.name = CoinsLabel.coinsLabelNodeName
        self.horizontalAlignmentMode = .left
        self.fontSize = 20
        self.zPosition = 0.2
        self.color = UIColor.white
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
