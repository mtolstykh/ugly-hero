//
//  Grawer.swift
//  Ugly Hero
//
//  Created by IOs Developer on 3/5/17.
//  Copyright © 2017 unlocked. All rights reserved.
//

import SpriteKit

class Grawer: MonsterNode {
    private static let AtlasName = "grawer-iddle"
    private static let TextureName = "grawer"
    
    init(position: CGPoint) {
        let grawerSize = CGSize(width: 120, height: 110)
        super.init(
            atlas: Grawer.AtlasName,
            texture: Grawer.TextureName,
            size: grawerSize,
            position: position
        )
        
        physicsBody = SKPhysicsBody(rectangleOf: size)
        physicsBody?.mass = 0.03
        physicsBody?.affectedByGravity = true
        physicsBody?.isDynamic = true
        physicsBody?.allowsRotation = false
        physicsBody?.categoryBitMask = PhysicsCategory.Monster
        physicsBody?.collisionBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero | PhysicsCategory.Bullet | PhysicsCategory.Monster
        physicsBody?.contactTestBitMask = PhysicsCategory.Floor | PhysicsCategory.Hero | PhysicsCategory.Bullet | PhysicsCategory.Monster
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
